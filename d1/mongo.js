/*

Using MongoDB locally:


To use MongoDB on local machine , you must first open a terminal and type "mongod"

server must always be running

open separate terminal and type "mongo" to open Mongo shell , which is a command line interface that allows us to issue MongoDB commands

//MongDB Commands:

//Show list of all databases
show dbs

//Create a database

//use <database name>

use myFirstDB

//THE USE COMMAND IS Tthe first step in creating a database.Next create a record



*/

//Crate a single document

db.users.insertOne({name: "John Smith", age: 20, isActive: true})

//get list of all users

db.users.find()

//Create a second user

db.users.insertOne({name: "Jane Doe", age: 23, isActive: false})

//Create a collection

//If a collection does not exist yet MongoDB creates the colection when you first store data for taht collection


db.products.insertMany([
 {name: "Product 1", price: 200.50, stock: 100, description: "Lorem Ipsum"},
 {name: "Product 2", price: 300, stock: 50, description: "Lorem Ipsum"}	
	])

//Find Specific document
db.products.find({name: "Product 2"})

//update field
db.users.updateOne(
	{_id:  ObjectId("61fbcb6c70b913ce8069f3b3") },
	{ $set: {isActive: true} } 
)

//update new field
db.users.updateOne(
	{_id:  ObjectId("61fbcb6c70b913ce8069f3b3") },
	{ $set: {
		address: {
       street: "123 new street",
       city: "New york",
       country: "united states"
	 } 

	} 
   } 
)

//delete field
db.users.deleteOne({_id: ObjectId("61fbcb6c70b913ce8069f3b3")})



