
//creating courses fields
db.courses.insertMany([
  {course:"basic HTML", description:"intro html course", price: 100, isActive: true, enrollees: ["ashley", "tadiwa", "tawanda"] },
  {course:"basic CSS", description:"intro CSS course", price: 200, isActive: true, enrollees: ["ashley", "Kudzy", "Mumue"] },
  {course:"basic Javascript", description:"intro JS course", price: 300, isActive: true, enrollees: ["ashley", "tadiwa", "tawanda"] }
	])

//updating js field

db.courses.updateOne(
	{_id:  ObjectId("61fbda5870b913ce8069f3b8") },
	{ $set: {
		enrollees: {
       userId: ObjectId("61fbda5870b913ce8069f3b8")
     
	 } 

	} 
   } 
)